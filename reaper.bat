@echo off

set exename=%1
set exename=%exename:"=%

:top
tasklist /FI "IMAGENAME eq %exename%" | findstr "%exename%" >nul

IF %ERRORLEVEL% == 0 GOTO kill
GOTO eof

:kill
taskkill /IM "%exename%" /F
echo killing task...
TIMEOUT 5
GOTO :top

:eof
echo task %exename% done
exit /b